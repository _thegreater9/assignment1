**Instructions on how to build install and use your project

open Visual studio
open the file location in visual studio
paste the html code in visual studio
Open the Html and css files for this page
Add a meta element for the viewport
Convert the fixed widths to fluid widths
Add a media query for a tablet in portrait view
Add a media query for a phone in landscape orientation
Add a media query for a phone in portrait orientation
Add a mobile menu using the Slicknav plugin
Use the developer tools to test the page
run the project in any browser

**Adding a license for a project 

MIT LIicense
MIT License is a license with conditions only requiring preservation of copyright and license notices.



